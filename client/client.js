console.log("Starting Client")

let agent = navigator.userAgent;

let host = window.location.host

let path = window.location.pathname

let language = navigator.language

let screen_height = window.screen.height
let screen_width = window.screen.width

let resolution = ""+screen_width+"x"+screen_height+""

let title = document.title

fetch("https://api.gotracker.local.jrehkemper.de/api/v1/userdata", {
    method: "post",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        agent: agent,
        host: host,
        path: path,
        language: language,
        resolution: resolution,
        title: title
  })
})
