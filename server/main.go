package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Visit struct {
	Id         uint   `json:"id"`
	Path       string `json:"path"`
	Host       string `json:"host"`
	Ip         string `json:"ip"`
	Agent      string `json:"agent"`
	Resolution string `json:"resolution"`
	Language   string `json:"language"`
	Title      string `json:"title"`
	Timestamp  time.Time
}

var DB *gorm.DB

func main() {
	godotenv.Load(".env")

	var err error
	/* Define connection string */

	env_username := os.Getenv("PG_USERNAME")
	env_password := os.Getenv("PG_PASSWORD")
	env_host := os.Getenv("PG_HOST")
	env_port := os.Getenv("PG_PORT")
	env_database := os.Getenv("PG_DATABASE")
	//dsn := "host=family-tasks-database user=gorm password=gorm dbname=gorm port=5432 sslmode=disable TimeZone=Europe/Berlin"
	dsn := "host=" + env_host + " user=" + env_username + " password=" + env_password + " dbname=" + env_database + " port=" + env_port + " sslmode=disable TimeZone=Europe/Berlin"

	/* Connect to Database */

	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	/* Handle Error */
	if err != nil {
		log.Fatal("Cannot connect to Database")
	}
	fmt.Println("Database connected")

	/* Auto Migration for Database */
	DB.AutoMigrate(Visit{})

	app := fiber.New()

	/* Initialize CORS config */
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
	}))

	app.Post("/api/v1/userdata", Userdata)

	app.Listen(":5000")
	fmt.Println("Server started on Port 5000")

}

func Userdata(c *fiber.Ctx) error {
	visit := new(Visit)

	err := c.BodyParser(&visit)
	if err != nil {
		log.Fatal(err)
	}

	ips := c.IPs()
	visit.Ip = ips[0]
	visit.Timestamp = time.Now()

	headers := c.GetReqHeaders()
	visit.Agent = headers["User-Agent"]

	/* Catch page in loading state */
	if visit.Agent == "undici" {
		c.SendStatus(200)
		return nil
	}

	results := DB.Create(visit)

	if results.Error != nil {
		log.Fatal(results.Error)
	}

	c.SendStatus(200)
	return nil
}
